#!/usr/bin/env python3

from chuffchuff import *

import tkinter as tk
from tkinter import ttk, messagebox
from tkcalendar import DateEntry
import csv

# See: https://stackoverflow.com/questions/47839813/python-tkinter-autocomplete-combobox-with-like-search

def on_keyrelease(event, listbox):

    # get text from entry
    value = event.widget.get()
    value = value.strip().lower()

    # get data from test_list
    if value == '':
        data = stations
    else:
        data = []
        for item in stations:
            if value in item.lower():
                data.append(item)

    # update data in listbox
    listbox_update(data, listbox)


def listbox_update(data, listbox):
    # delete previous data
    listbox.delete(0, 'end')

    # sorting data
    data = sorted(data, key=str.lower)

    # put new data
    for item in data:
        listbox.insert('end', item)


FROM = ""
TO = ""
start_date = ""
window = 0
outf = ""


def check_and_exit(event):
    global FROM, TO, start_date, window, outf
    if len(from_listbox.curselection()) == 0:
        messagebox.showinfo("Error", "Please select a valid departure station")
        return
    else:
        FROM = from_listbox.get(from_listbox.curselection())[-4:-1]

    if len(to_listbox.curselection()) == 0:
        messagebox.showinfo("Error", "Please select a valid arrival station")
        return
    else:
        TO = to_listbox.get(to_listbox.curselection())[-4:-1]
    start_date = cal.get_date()       # datetime.date
    try:
        window = int(window_entry.get())
    except ValueError:
        messagebox.showinfo("Error", "Please select a valid integer window")
        return
    if outf_entry.get() == "":
        messagebox.showinfo("Error", "Please select a valid output filename")
        return
    else:
        outf = outf_entry.get()
    root.destroy()
    
# --- main ---

stations = []

with open("station_codes.csv", "r") as csvfile:
    csvreader = csv.reader(csvfile)
    next(csvreader)
    for row in csvreader:
        stations.append(row[0] + " (" + row[1] + ")")

    arrival_stations = stations.copy()

# layup window
root = tk.Tk()
style = ttk.Style(root)
style.theme_use('clam')

from_label = tk.Label(root, text="Departure Station")
from_label.grid(row=0, column=0)
from_entry = tk.Entry(root)
from_entry.grid(row=1, column=0)
from_listbox = tk.Listbox(root, exportselection=0)
from_listbox.grid(row=2, column=0)
listbox_update(stations, from_listbox)
from_entry.bind(
    '<KeyRelease>', lambda event, lb=from_listbox: on_keyrelease(event, lb))

to_label = tk.Label(root, text="Arrival Station")
to_label.grid(row=0, column=1)
to_entry = tk.Entry(root)
to_entry.grid(row=1, column=1)
to_listbox = tk.Listbox(root, exportselection=0)
to_listbox.grid(row=2, column=1)
listbox_update(stations, to_listbox)
to_entry.bind(
    '<KeyRelease>', lambda event, lb=to_listbox: on_keyrelease(event, lb))

cal_label = tk.Label(root, text="Date")
cal_label.grid(row=0, column=2)
cal = DateEntry(root, width=12)
cal.grid(row=1, column=2)

window_label = tk.Label(root, text="Window length")
window_label.grid(row=0, column=3)
window_entry = tk.Entry(root)
window_entry.grid(row=1, column=3)

outf_label = tk.Label(root, text="CSV File Name")
outf_label.grid(row=0, column=4)
outf_entry = tk.Entry(root)
outf_entry.insert(0, "~/Desktop/trains.csv")
outf_entry.grid(row=1, column=4)

# outf = filedialog.asksaveasfilename(initialdir = "~/Desktop", title="Output (CSV) file",filetypes = (("csv files","*.csv"),("all files","*.*")))
# print(outf)

submit = tk.Button(root, text="Run")
submit.grid(row=1, column=5)
submit.bind('<Button-1>', check_and_exit)

root.mainloop()

trains = parallel_profile_days(FROM, TO, start_date, search_days=window)

save_csv(trains, outf)

plot_trains(trains)
print(FROM, TO, start_date, window, outf)
