#!/usr/bin/python3
import requests
import sys
from os import path
from time import sleep

from datetime import datetime, date, timedelta
from datetime import time as tme
import threading
import queue

from bs4 import BeautifulSoup
import json
from collections import OrderedDict
import matplotlib.pyplot as plt
import csv
import argparse
from dateutil import parser as date_parser
import copy


def safe_fetch(url):
    """Fetch url with timeout and retries, handling errors with
exponential backoff of up to 5s.  Ulimately, run exit code if it all
goes wrong.
    """
    attempts = 0
    while attempts < 10:
        try:
            r = requests.get(url, timeout=3)
            return (r)
        except requests.exceptions.RequestException as e:
            print(e)
            attempts += 1
            delay = attempts * 0.5
            print("waiting %s s for reconnect" % delay)
            sleep(delay)  # exponential backoff
    byebye(error=True)  # only here if 10 attempts fail


def byebye(error=False):
    print('byebye')
    if error is True:
        sys.exit(1)
    else:
        sys.exit(0)


def generate_url(depart, destination, when, departing=True):
    """Generate a url to search nationalrail's database with.  Parameters
are:
    * depart: 3-letter station departure code
    * destination : 3-letter station arrival code
    * when: datetime.datetime object for departure date/time
"""
    if departing is True:
        da = "dep"
    else:
        da = "arr"
    url = "http://ojp.nationalrail.co.uk/service/timesandfares/%s/%s/%s/%s/%s" % (
        depart, destination, when.strftime("%d%m%y"), when.strftime("%H%M"),
        da)
    return (url)


def get_at_dict(obj):
    """Sometimes the dictionary is buried in a list.  This delves down if
needed"""
    if type(obj) is dict:
        return (obj)
    else:
        try:
            return (obj[0])  # seems to work for now
        except IndexError:
            return None  # no dict to get at = no ticket


def fares(soup, one_way=True, departure=True):
    """Return dictionary of fares, in format:
    * time: {props_dict}
where props_dict is """
    trains = OrderedDict()

    for i in soup.find_all('td', class_='fare'):
        i = i.script.contents[0]
        data = json.loads(i.strip())

        journey = get_at_dict(data['jsonJourneyBreakdown'])
        single_ticket = get_at_dict(data['singleJsonFareBreakdowns'])
        return_ticket = get_at_dict(data['returnJsonFareBreakdowns'])
        k = string_to_time(journey['departureTime'])
        if one_way is True:
            ticket = single_ticket
        else:
            ticket = return_ticket
        trains[k] = {
            'From':
            journey["departureStationCRS"],
            'From_name':
            journey["departureStationName"],
            'To':
            journey["arrivalStationCRS"],
            'To_name':
            journey["arrivalStationName"],
            'Depart':
            journey["departureTime"],
            'Arrive':
            journey["arrivalTime"],
            'Changes':
            journey["changes"],
            'Duration':
            str(journey["durationHours"]) + ":" +
            str(journey["durationMinutes"]),
            'Price':
            ticket["fullFarePrice"],
            'Ticket Type':
            ticket["fareTicketType"],
            'Train Company':
            ticket["fareProvider"]
        }
    print("found %i trains" % len(trains))
    return (trains)


def string_to_time(s):
    return (tme(int(s.split(':')[0]), int(s.split(':')[1])))


def get_days_fares(depart, destination, date):
    """Get dictionary of entire days' available fares.  Return
concatenated dictionaries"""
    train_time = tme(0, 0)  # start early...
    days_fares = OrderedDict()
    end_of_day = False
    while not end_of_day:
        url = generate_url(
            depart,
            destination,
            datetime.combine(date, train_time),
            departing=True)

        soup = BeautifulSoup(safe_fetch(url).text, features="lxml")
        trains = fares(soup)
        last_departure = False
        for train in trains:
            if last_departure is False:
                # first result---assume today until I find a better way
                days_fares[train] = trains[train]
                last_departure = string_to_time(trains[train]['Depart'])
            elif string_to_time(trains[train]['Depart']) > last_departure:
                days_fares[train] = trains[train]
                last_departure = string_to_time(trains[train]['Depart'])
            else:
                print('reached end of day')
                end_of_day = True
                break  # no need to parse others
            train_time = (datetime.combine(date.today(), last_departure) +
                          timedelta(seconds=60)).time()  # next batch
            # last time in day for next departure time (sort dict.keys())
    return (days_fares)


def linear_profile_days(depart, destination, start_date, search_days=0):
    """Return dictionary of all fares in window
start_date..start_date+days
    * depart: 3-letter station departure code
    * destination : 3-letter station arrival code
    * start_date: datetime.date object for departure date
    * search_days: int of number of days to profile.  default is 7.
"""
    days_fares = OrderedDict()
    for search_day in range(search_days + 1):
        today = start_date + timedelta(days=search_day)
        print("profiling %s" % str(today))
        f = get_days_fares(depart, destination, today)
        for key in f:
            days_fares[key] = f[key]
    return (days_fares)


all_fares = {}  # we order it later


# Make consumer threads.
class ThreadProfileTrains(threading.Thread):
    def __init__(self, train_queue):
        threading.Thread.__init__(self)
        self.train_queue = train_queue
        # print some stuff to stay we started
        print('Begun thread')
        self._open = True

    def run(self):
        global all_fares
        while self._open:
            # Grab a train from train_queue.
            train = self.train_queue.get()
            # And process it.
            all_fares[train['date']] = linear_profile_days(
                train['depart'],
                train['destination'],
                train['date'],
                search_days=0)
            # Mark the train_queue job as done.
            self.train_queue.task_done()

    def close(self):
        print("Closing", self)
        self._open = False


def parallel_profile_days(depart,
                          destination,
                          start_date,
                          search_days=1,
                          parallel_threads=5):
    """Return dictionary of all fares in window
start_date..start_date+days
    * depart: 3-letter station departure code
    * destination : 3-letter station arrival code
    * start_date: datetime.date object for departure date
    * search_days: int of number of days to profile.  default is 7.

Runs parallel_threads instances of linear_profile_days in parallel

Return:
    {date:{trains}}
"""
    train_queue = queue.Queue()
    for day in range(search_days + 1):
        date = start_date + timedelta(days=day)
        train_queue.put({
            'depart': depart,
            'destination': destination,
            'date': date
        })
    threads = [
        ThreadProfileTrains(train_queue) for _ in range(parallel_threads)
    ]
    for t in threads:
        t.setDaemon(True)
        t.start()

    # Wait until the train_queue is empty, then close the threads.
    train_queue.join()
    for t in threads:
        t.close()
    global all_fares
    ordered_dates = sorted(list(all_fares.keys()))
    trains = OrderedDict()
    for d in ordered_dates:
        trains[d] = all_fares[d]
    return (trains)


def save_csv(trains, csvfile):
    """Save all that hard work to a csvfile"""
    csvfile = path.expanduser(csvfile)
    with open(csvfile, 'w') as f:
        fieldnames = [
            'Date', 'From', 'To', 'Depart', 'Arrive', 'Price', 'Changes',
            'Duration', 'Ticket Type', 'Train Company'
        ]
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        local_trains = copy.deepcopy(trains)
        for day in local_trains:
            for train in local_trains[day]:
                local_trains[day][train].pop('To_name')
                local_trains[day][train].pop('From_name')
                writer.writerow({'Date': day, **local_trains[day][train]})


def plot_trains(trains):
    prices = []
    times = []
    for day in trains:
        for train in trains[day]:
            times.append(datetime.combine(day, train))
            prices.append(trains[day][train]['Price'])
    plt.plot(times, prices)
    plt.xlabel("Departure Date and Time")
    plt.ylabel("Ticket Price")
    first_day = next(iter(trains))
    first_train = trains[first_day][next(iter(trains[first_day]))]
    plt.title("Ticket Prices from %s to %s between %s and %s" %
              (first_train['From_name'], first_train['To_name'],
               str(first_day),
               str(first_day + timedelta(len(list(trains.keys()))))))
    plt.xticks(rotation=90)
    plt.show()
    fig = plt.gcf()
    fig.autofmt_xdate()

                
# ====================================================================
# Execution starts here
# ====================================================================

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("FROM", help="Departure Station 3-letter code")
    parser.add_argument("TO", help="Arrival Station 3-letter code")
    parser.add_argument(
        "-d",
        "--date",
        help="Date of beginning of window in ISO format",
        required=True)
    parser.add_argument(
        "-w",
        "--window_length",
        type=int,
        help="Window length in days.  0 = only today",
        required=True)
    actions = parser.add_argument_group('Actions')
    actions.add_argument("-p", "--plot", help="Plot", action="store_true")
    load_save = actions.add_mutually_exclusive_group()
    load_save.add_argument("-o", "--out", help="File to save output to")
    load_save.add_argument("-i", "--read", help="Read data from file")
    args = parser.parse_args()

    at_least_one = [args.out, args.plot]  # actions required to do something
    if not any(at_least_one):
        print('Must specify at least one action!')
        sys.exit(1)

    start_date = date_parser.parse(args.date).date()

    if args.read is not None:
        print('reading from csv not implemented yet')
        sys.exit(1)

    trains = parallel_profile_days(
        args.FROM, args.TO, start_date, search_days=args.window_length)

    if args.out is not None:
        save_csv(trains, args.out)

    if args.plot is True:
        plot_trains(trains)

        
